import { RefObject, useEffect } from "react";


function useOutsideAlerter(ref: RefObject<HTMLElement>, alerter: (e?: Event)=>void) {
    const handleClickOutside=(event: MouseEvent)=>{
        if (ref?.current && !ref?.current?.contains(event.target as Node)) {
            alerter(event);
        }
    }
    
    useEffect(() => {   
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);
}

export default useOutsideAlerter;