import api from '@helpers/api';
import { AxiosRequestConfig } from 'axios';
import { useState } from 'react';

//HTTP Methods
export const GET = 'get';
export const POST = 'post';
export const PATCH = 'patch';
export const DELETE = 'delete';
export const PUT = 'put';


type Method = typeof GET | typeof POST | typeof PUT | typeof DELETE | typeof DELETE | typeof PATCH;

const useFetch = <DataType = any>(method: Method, url: string) => {
    const [data, setData] = useState<DataType | null>(null);
    const [error, setError] = useState<Error | null>(null);
    const [loading, setLoading] = useState(true);

    const clear = () => {
        setData(null);
        setLoading(true);
        setError(null);
    }

    const call = async (options?: AxiosRequestConfig): Promise<DataType|null> => {
        let result = null;
        try {
            clear();
            const response = await api.request<DataType>({
                ...options,
                method,
                url,
            });
            if (response.status === 200) {
                setData(response.data);
                result = response.data;
            }
            else {
                throw new Error(`Error ${response.status} : ${response.statusText}`);
            }
        }
        catch (error) {
            setError(error as Error);
        }
        finally {
            setLoading(false);
            return result;
        }
    };

    return { call, data, error, loading, clear };
};

export default useFetch;