import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import MainPage from './pages/MainPage';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import { HOME } from '@helpers/routes';
import { ErrorBoundary } from '@components/error';

const router = createBrowserRouter([
  {
    path: HOME,
    element: <MainPage />,
    errorElement: <ErrorBoundary />
  }
]);

function App() {
  return (
    <Provider store={store}>
        <RouterProvider router={router} />
    </Provider>
  )
}

export default App
