import imedia24Logo from "@assets/imedia24.png";
import PokemonList from "@components/pokemonList";

function MainPage(){
    return (
        <>
            <div>
                <a href="https://react.dev" target="_blank">
                <img src={imedia24Logo} className="logo react" alt="React logo" />
                </a>
            </div>
            <h1 id="app-title">Pokemon Application</h1>
            <PokemonList />
        </>
    )
}

export default MainPage;