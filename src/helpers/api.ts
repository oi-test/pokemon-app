import axios from 'axios';

export const API_URL="https://pokeapi.co/api/v2";
export const POKEMON_PATH="/pokemon";
export const POKEMON_DETAILS_PATH="/pokemon/:id";

//HTTP Request Headers
const defaultHeaders = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}

export default axios.create({
    baseURL: API_URL,
    headers: defaultHeaders
});