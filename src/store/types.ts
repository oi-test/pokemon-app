export enum ActionTypes {
    ADD_POKEMON = 'ADD_POKEMON',
    REMOVE_POKEMON = 'REMOVE_POKEMON',
    LOAD_REQUEST = 'LOAD_REQUEST',
    LOAD_SUCCESS = 'LOAD_SUCCESS',
    LOAD_FAILURE = 'LOAD_FAILURE'
}

export interface PokemonActionTypes {
    type: ActionTypes,
    payload: Pokemon
}

export interface Pokemon {
    id: number,
    name: string
}
  
/**
 * State types
 */
export interface ApplicationState {
    pokemons: PokemonsState,
}
  
export interface PokemonsState {
    readonly data: Pokemon[];
    readonly currentOffset: number;
    readonly loading: boolean;
}