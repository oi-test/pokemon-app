import { PokemonsState, ApplicationState } from '@store/types';
import { createSlice } from '@reduxjs/toolkit'
import { POKEMONS_CALL_LIMIT } from '@helpers/consts';

const initialState: PokemonsState = {
  data: [],
  currentOffset: 1,
  loading: false
};

const pokemonSlice = createSlice({
  name: 'pokemons',
  initialState,
  reducers: {
    loadRequest(state) {
      return ({ ...state, loading: true });
    },
    loadMoreRequest(state){
      return ({ ...state, loading: true });
    },
    loadSuccess(state, action){
      return ({
        ...state, 
        data: action.payload, 
        loading: false
      });
    },
    loadMoreSuccess(state, action){
      return ({
        ...state, 
        currentOffset: state.currentOffset+POKEMONS_CALL_LIMIT, 
        data: [...state.data, ...action.payload], 
        loading: false
      });
    },
    loadFailure(state){
      return ({ ...state, data: [], loading: false });
    }
  }
});

export const pokemonSelector = (state: ApplicationState) =>state.pokemons.data;

export const loadingSelector = (state: ApplicationState) =>state.pokemons.loading;

export const { loadRequest, loadMoreRequest, loadSuccess, loadMoreSuccess, loadFailure } = pokemonSlice.actions;

export default pokemonSlice.reducer;