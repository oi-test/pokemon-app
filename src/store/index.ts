import { configureStore } from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga';
import pokemons from './reducers/pokemons';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    pokemons,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware)
});

sagaMiddleware.run(rootSaga);

export default store;