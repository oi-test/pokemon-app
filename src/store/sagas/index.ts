import { all, select, takeLatest } from 'redux-saga/effects';
import { call, put } from 'redux-saga/effects';
import api, { API_URL, POKEMON_PATH } from '@helpers/api';
import { loadFailure, loadMoreRequest, loadMoreSuccess, loadRequest, loadSuccess } from '@store/reducers/pokemons';
import { ApplicationState } from '@store/types';
import { POKEMONS_CALL_LIMIT } from '@helpers/consts';

const extractPokemonId=(url: string)=>{
  let id: number = 0;
  const regexPattern = new RegExp(`^${API_URL+POKEMON_PATH}/(\\d+)/`);
  const match = url.match(regexPattern);
  if (match) {
    id = parseInt(match[1]);
  }
  return id;
}

type PokemonApiData = {
  data: {
    count: number;
    results: Array<{ name: string, url: string }>
  }
}

//To simulate loading (ONLY FOR TESTING PURPOSES)
function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function* load() {  
  try {
    const initialOffset = 1;
    const { data }: PokemonApiData = yield call(
      api.get, 
      POKEMON_PATH,
      {
        params: {
          offset: initialOffset,
          limit: POKEMONS_CALL_LIMIT
        }
      }
    );
    const pokemonsData = data.results.map(({ name, url })=>({name, id: extractPokemonId(url)}))
    yield put(loadSuccess(pokemonsData));
  } catch (error) {
    yield put(loadFailure());
  }
}

export function* loadMore() {  
  try {
    const selectNextOffset = (state: ApplicationState) => state.pokemons.currentOffset;
    const currentOffset: number = yield select(selectNextOffset); 

    const { data }: PokemonApiData = yield call(
      api.get, 
      POKEMON_PATH,
      {
        params: {
          offset: currentOffset + POKEMONS_CALL_LIMIT,
          limit: POKEMONS_CALL_LIMIT
        }
      }
    );
    const pokemonsData = data.results.map(({ name, url })=>({name, id: extractPokemonId(url)}));

    //Set a delay of 400 ms before updating state to test loading
    yield delay(400);

    yield put(loadMoreSuccess(pokemonsData));
  } catch (error) {
    yield put(loadFailure());
  }
}

export default function* rootSaga(): Generator {
    return yield all([
      takeLatest(loadRequest, load),
      takeLatest(loadMoreRequest, loadMore)
    ]);
}