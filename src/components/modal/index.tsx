import useOutsideAlerter from "@hooks/useOutsideAlerter";
import styles from "./Modal.module.css";
import { ModalProps } from "./types";
import { useRef } from "react";


export function Modal({open, onClose, children, className}: ModalProps){
    const modalRef =  useRef<HTMLInputElement>(null);
    useOutsideAlerter(modalRef, onClose);
    
    return(
        <div className={styles.modalContainer} style={{visibility: open  ? "visible" : "hidden"}}>
            <div 
                ref={modalRef} 
                className={`${styles.modal} ${className || ''}`}  
                style={{opacity: open ? 1 : 0, transition: "all .4s", visibility: open ? "visible" : "hidden"}}
            >
                <button className={styles.closeBtn} onClick={onClose}>
                    X
                </button>
                <div className={styles.content}>
                    {children}
                </div>
            </div>
        </div>
    );
}
