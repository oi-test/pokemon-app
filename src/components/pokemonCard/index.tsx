import pokeball from "@assets/pokeball.svg";
import { PokemonCardProps } from "./types";
import styles from "./PokemonCard.module.css";

function PokemonCard({ pokemon: {name, id}, onClick }: PokemonCardProps){
    const handleClick=()=>{
        onClick(id);
    }
    
    return (
        <div data-testid="pokemon-card" className={`${styles.card} pokemon-card`} onClick={handleClick}>
            <img src={pokeball} width={50} alt="Pokemon Card Image" />
            <h2 data-testid="pokemon-name">{name}</h2>
        </div>
    );
}

export default PokemonCard;