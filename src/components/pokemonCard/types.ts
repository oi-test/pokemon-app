export interface PokemonCardProps {
    pokemon: {
        name: string,
        id: number
    },
    onClick: (id: number)=>void;
}