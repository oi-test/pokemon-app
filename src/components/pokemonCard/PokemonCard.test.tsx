// App.test.js
import { fireEvent, render, screen } from "@testing-library/react";

import PokemonCard from "@components/pokemonCard";


describe("PokemonCard", () => {
    it("Renders PokemonCard component", () => {
        const props={
            pokemon: {
                id: 1,
                name: "test"
            },
            onClick: jest.fn(),
        }
    
        render(<PokemonCard {...props} />);
        expect(screen.getByTestId("pokemon-name").textContent).toBe(props.pokemon.name);
    });

  it("Test click PokemonCard", () => {
    const props={
        pokemon: {
            id: 1,
            name: "test"
        },
        onClick: jest.fn(),
    }

    render(<PokemonCard {...props} />);
    fireEvent.click(screen.getByTestId("pokemon-card"));
    expect(props.onClick).toBeCalled();
  });
});