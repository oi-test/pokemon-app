export interface PokemonDetailsProps {
    id: number|null;
    onClearId: ()=>void;
}

export interface PokemonDetailsData {
    id: number;
    name: string;
    moves: Array<{ move: { name: string }}>;
    sprites: { front_default: string };
}