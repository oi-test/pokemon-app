import { Modal } from "@components/modal";
import { PokemonDetailsData, PokemonDetailsProps } from "./types";
import useFetch, { GET } from "@hooks/useFetch";
import { POKEMON_DETAILS_PATH } from "@helpers/api";
import { useEffect } from "react";

 
const PokemonDetails = ({ id, onClearId }: PokemonDetailsProps) => {
    const { call, data } = useFetch<PokemonDetailsData>(GET, POKEMON_DETAILS_PATH.replace(':id', String(id)));

    useEffect(()=>{
        const controller = new AbortController();
        if(id){
            call({ signal: controller.signal });
        }
        return function(){
            controller.abort();
        }
    }, [id]);

    return (
        <Modal open={id!==null} onClose={onClearId}>
            <div className="pokemon-details">
                <img src={data?.sprites.front_default} width={70} />
                <h3>
                    Hello I'm {data?.name}
                </h3>
                <p>Some of my cool moves: {data?.moves.slice(0, 10).map(({ move })=>move.name).join(", ")}</p>
            </div>        
        </Modal>
    );
}
 
export default PokemonDetails;