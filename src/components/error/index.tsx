import { isRouteErrorResponse, useRouteError } from "react-router-dom";
import styles from "./Error.module.css";

export function ErrorBoundary(){
    const error = useRouteError();

    let errorMessage: string;

    if (isRouteErrorResponse(error)) {
        // error is type `ErrorResponse`
        errorMessage = error.statusText;
    } else if (error instanceof Error) {
        errorMessage = error.message;
    } else if (typeof error === 'string') {
        errorMessage = error;
    } else {
        errorMessage = 'Unknown error';
    }

    function reloadPage() {
        location.reload();
    }

    return(
        <div className={styles.errorContainer}>
            <div className={styles.error}>
                <span className={styles.errorIcon} />
                <h1>Oops! you have an error</h1>
                <p>{errorMessage}</p>
                <button onClick={reloadPage}>
                    Reload
                </button>
            </div>
        </div>
    );
}