import PokemonCard from "@components/pokemonCard";
import { loadMoreRequest, loadRequest, loadingSelector, pokemonSelector } from "@store/reducers/pokemons";
import { UIEventHandler, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "./PokemonList.module.css";
import PokemonDetails from "@components/pokemonDetails";
import Loader from "@components/loader";

function PokemonList() {
    const data = useSelector(pokemonSelector);
    const loading = useSelector(loadingSelector);

    const dispatch = useDispatch();

    const [selectedPokemonId, setSelectedPokemonId] = useState<number|null>(null);

    useEffect(()=>{
        dispatch(loadRequest());
    }, []);

    const handleScroll: UIEventHandler<HTMLDivElement>=(event)=>{
        //Trigger dispatch before 2px of the end of scroll
        const SCROLL_DIFF = 2;
        const target = event.target as HTMLDivElement;
        if (target.clientHeight + target.scrollTop + SCROLL_DIFF  >= target.scrollHeight) {
            dispatch(loadMoreRequest());
        }
    }

    const handleClearId=()=>{
        setSelectedPokemonId(null);
    }

    const handleClickPokemon=(id: number)=>{
        setSelectedPokemonId(id);
    }

    return (
        <div className={styles.container}>
            {loading && <Loader />}
            <div className={styles.pokemonList} onScroll={handleScroll}>
                <PokemonDetails id={selectedPokemonId} onClearId={handleClearId} />
                {data.map((pokemon)=>
                    <PokemonCard 
                        key={pokemon.id} 
                        pokemon={pokemon} 
                        onClick={handleClickPokemon} 
                    />
                )}
            </div>
        </div>
    );
}

export default PokemonList;