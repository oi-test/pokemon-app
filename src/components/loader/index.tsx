import styles from "./Loading.module.css";
import pokeball from "@assets/pokeball.svg";

const Loader = () => {
    return (
        <div className={styles.loaderContainer}>
            <img className={styles.spinner} src={pokeball} width={100} alt="Loader Spinner Icon" />
        </div>
    );
}
 
export default Loader;