from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import time

browser = webdriver.Chrome()
browser.get("http://localhost:3000")

MAX_WAIT_TIME = 5

def test_app_running():
    title_elem = browser.find_element(By.ID, "app-title")
    assert title_elem.text == 'Pokemon Application'

def test_click_pokemon_card():
    WebDriverWait(browser, MAX_WAIT_TIME).until(
        EC.presence_of_all_elements_located([By.CLASS_NAME, "pokemon-card"])
    )
    pokemonCard = browser.find_element(By.CLASS_NAME, "pokemon-card") 
    pokemonCard.click()

    WebDriverWait(browser, MAX_WAIT_TIME).until(
        EC.presence_of_element_located([By.CLASS_NAME, "pokemon-details"])
    )

test_app_running()
test_click_pokemon_card()

print("All the e2e tests are passed")

time.sleep(1)
browser.close()