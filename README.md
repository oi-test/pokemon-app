
# Pokemon Application

A simple application that uses the api Poke API v2 to show the list of Pokemons.

## Getting Started

### Prerequisites
Make sure you have Node.js and npm (Node Package Manager) installed on your machine.

- Node.js: [https://nodejs.org/](https://nodejs.org/)
- npm: [https://www.npmjs.com/](https://www.npmjs.com/)
- python (For E2E Tests): [https://www.python.org/](https://www.python.org/)

### Installation

1. Clone this repository to your local machine:

   ```git clone git@gitlab.com:oi-test/pokemon-app.git```

2. Install the project dependencies:

   ```yarn```

3. (Optional) for e2e test using selenium python install the dependencies:

   ```pip install -r e2e-test/requirements.txt```


### Development
To start the development server and run the app locally, use the following command:

   ```yarn dev```

This will launch the development server and open the app in your default web browser. The app will automatically reload when you make changes to the source code.

### Production Build
To create a production-ready build of your app, use the following command:

   ```yarn build```

This will generate an optimized production build in the ``dist`` directory.

### Unit Tests
You can run unit tests of your application using the following command:

   ```yarn test```

### E2E Tests
This will execute the tests and provide you with the test results.

You can run E2E tests of your application using the following command:

   ```python e2e-test```

This will open chrome browser and execute the tests then provide you with the test results in the console.







