export default {
    preset: 'ts-jest',
    testEnvironment: 'jest-environment-jsdom',
    transform: {
        "^.+\\.tsx?$": "ts-jest" 
    },
    moduleNameMapper: {
        '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/test/mocks/fileMock.js',
        '^.+\\.(css|less)$': '<rootDir>/test/mocks/CSSMock.js',
        '^@components/(.*)$': '<rootDir>/src/components/$1',
        '^@helpers/(.*)$': '<rootDir>/src/helpers/$1',
        '^@assets/(.*)$': '<rootDir>/src/assets/$1',
        '^@hooks/(.*)$': '<rootDir>/src/hooks/$1',
        '^@pages/(.*)$': '<rootDir>/src/pages/$1',
        '^@store/(.*)$': '<rootDir>/src/store/$1',
    },
    roots: ["<rootDir>/src"],
    modulePaths: ["<rootDir>/src"],
    moduleDirectories: ["node_modules", "src"]
}